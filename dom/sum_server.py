# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
server_socket = socket.socket()
# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31006)
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

while True:
    # Czekanie na połączenie
    print('Server is up and waiting for connections')
    connection, client_address = server_socket.accept()

    try:
        # Odebranie danych i odesłanie ich spowrotem
        num1 = connection.recv(128)
        print('Received first number: ' + num1)

        num2 = connection.recv(128)
        print('Received second number: ' + num2)

        print('Calculating...')
        try:
            sum = int(num1) + int(num2)
        except:
            sum="At least one of inputs is not proper number, so I can't add them..."
        connection.send(str(sum))
        print('Sent results of calculations to client')

        pass

    finally:
        # Zamknięcie połączenia
        connection.close()

        pass