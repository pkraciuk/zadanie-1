# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
gniazdo = socket.socket()

# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31006)
gniazdo.connect(server_address)

try:
    # Wysłanie danych
    message = u'To jest wiadomosc, ktora zostanie zwrocona.'
    gniazdo.send(message.encode('utf-8'))
    # Wypisanie odpowiedzi
    response = gniazdo.recv(4096)
    print(response)

finally:
    # Zamknięcie połączenia
    gniazdo.close()
    pass
print("Done client")